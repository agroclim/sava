package fr.agroclim.sava.sample;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.agroclim.sava.core.MetricsBasicAuthServlet;
import fr.agroclim.sava.core.SavaUtils;
import lombok.extern.log4j.Log4j2;

/**
 * An example of metrics servlet to use SAVA.
 * @author ltromel
 */
@Log4j2
@WebServlet("/metrics")
public final class ExampleMetricsServlet extends MetricsBasicAuthServlet {

    /**
     * UID.
     */
    private static final long serialVersionUID = 7268179741439922672L;

    @Override
    public void init() throws ServletException {
        super.init();
        LOGGER.info("Metrics servlet has started.");

        final Callable<Double> getRandom = () -> Math.random();
        SavaUtils.addGauge("random", "Get a new random double every 5 secondes", getRandom, 5, TimeUnit.SECONDS);
        SavaUtils.addGauge("constant", "Constant of '1993,0' value, labeled 'constant label'", (double) 1993,
                "constant_getter", "method_description");
        SavaUtils.addGauge("another_constant", "Constant of '2023,0' value, without label", (double) 2023);
        SavaUtils.addGauge("decrement_test", "Decrements from 100 with step 5", (double) 100);
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        super.doGet(req, resp);
        SavaUtils.setGaugeValue("decrement_test", SavaUtils.getGaugeValue("decrement_test") - 5);
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        super.doPost(req, resp);
    }
}
