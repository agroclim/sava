package fr.agroclim.sava.sample;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.log4j.Log4j2;

/**
 * Ping servlet. Responds a pong text.
 * @author ltromel
 */
@Log4j2
@WebServlet("/ping")
public class PongServlet extends HttpServlet {

    /**
     * UID.
     */
    private static final long serialVersionUID = -4720570192463321838L;

    /**
     * Get a pong for any ping.
     */
    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
        throws ServletException, IOException {
        LOGGER.info("ping");
        resp.getWriter().append("pong");
        resp.setContentType("text/html; charset=UTF-8");
    }

}
