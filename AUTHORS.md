# Authors

## Louis Tromel
- [GitHub](https://github.com/Ash84)
- GitLab : [@louis.tromel](https://forgemia.inra.fr/louis.tromel)
- [ORCID](https://orcid.org/0000-0002-8915-8386)

## Olivier Maury
- GitLab : [@olivier.maury](https://forgemia.inra.fr/olivier.maury)
- [ORCID](https://orcid.org/0000-0001-9016-9720)
