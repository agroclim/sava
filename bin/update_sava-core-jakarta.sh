#!/bin/bash
DIR=$(dirname $0)/../
JAKARTA_DIR=$DIR/sava-core-jakarta
rm -fr $JAKARTA_DIR
cp -r $DIR/sava-core $JAKARTA_DIR
sed -i -e "s:<artifactId>sava-core</artifactId>:<artifactId>sava-core-jakarta</artifactId>:g" $JAKARTA_DIR/pom.xml
sed -i -e "s/javax.servlet/jakarta.servlet/g" $JAKARTA_DIR/pom.xml
sed -i -e "s:<name>SAVA library</name>:<name>SAVA library for Jakarta</name>:g" $JAKARTA_DIR/pom.xml
sed -i -e "s:<servlet-api.version>3.1.0</servlet-api.version>:<servlet-api.version>5.0.0</servlet-api.version>:g" $JAKARTA_DIR/pom.xml
sed -i -e "s:<artifactId>simpleclient_servlet</artifactId>:<artifactId>simpleclient_servlet_jakarta</artifactId>:g" $JAKARTA_DIR/pom.xml
find $JAKARTA_DIR -name "*.java" | xargs sed -i -e "s/javax.servlet/jakarta.servlet/g"
find $JAKARTA_DIR -name "*.java" | xargs sed -i -e "s/io.prometheus.client.exporter/io.prometheus.client.servlet.jakarta.exporter/g"

