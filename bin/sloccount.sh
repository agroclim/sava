#!/bin/bash
ROOT_DIR=$(dirname $(dirname $0))
TOKEI2CLOC=$(dirname $0)/tokei2cloc.py
TOKEI=$(command -v tokei)
if [ ! -x "$TOKEI" ]; then
	TOKEITGZ=tokei-x86_64-unknown-linux-gnu.tar.gz
	if [ ! -f bin/tokei ]; then
		mkdir -p ~/tmp
		if [ ! -f ~/tmp/$TOKEITGZ ]; then
			wget "https://github.com/Aaronepower/tokei/releases/download/v12.1.2/$TOKEITGZ" -O ~/tmp/$TOKEITGZ
		fi
		tar zxf ~/tmp/$TOKEITGZ -C bin
	fi
	TOKEI=bin/tokei
fi
if [ ! -f $TOKEI ]; then
	echo "Strange, $TOKEI does not exist!"
	exit 1
fi

echo "tokei is installed at $TOKEI"
$TOKEI --version
mkdir -p $ROOT_DIR/target
$TOKEI -f -o json $ROOT_DIR/sava-core/src $ROOT_DIR/sava-core-jakarta/src $ROOT_DIR/sava-example/src | $TOKEI2CLOC > $ROOT_DIR/target/cloc.xml
