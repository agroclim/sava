package fr.agroclim.sava.core;

import java.io.IOException;

import io.prometheus.client.Histogram;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.log4j.Log4j2;

/**
 * Initialize, register an {@link Histogram} metric that times every request
 * going in any servlet of the application.
 */
@Log4j2
@WebFilter(urlPatterns = { "/*" }, initParams = { @WebInitParam(name = "http.request.precision", value = "min"),
                        @WebInitParam(name = "http.request.buckets", value = "0.0001;0.0005;0.001;0.005;0.01;0.1;1") })
public class SavaFilter implements Filter {

    /**
     * Http request histogram collector.
     */
    private Histogram histogram;

    /**
     * Path precision.
     */
    private PathPrecision pathPrecision = PathPrecision.MAX;

    @Override
    public void destroy() {
        // Nothing to do.
    }

    /**
     * Time and register by method and path every request that come to any servlet
     * of the application. These values are next exposed on the /metrics servlet
     * with the name "http_request".
     */
    @Override
    public final void doFilter(final ServletRequest req, final ServletResponse resp, final FilterChain filterChain)
            throws IOException, ServletException {
        Histogram.Timer timer = null;
        if (req instanceof HttpServletRequest) {
            final HttpServletRequest httpRequest = (HttpServletRequest) req;
            String path = httpRequest.getServletPath();
            if (PathPrecision.MAX.equals(pathPrecision)) {
                final String cp = httpRequest.getContextPath();
                path = httpRequest.getRequestURI().substring(cp.length());
            }
            timer = histogram.labels(path, httpRequest.getMethod()).startTimer();
        }
        filterChain.doFilter(req, resp);
        if (timer != null) {
            timer.close();
        }
    }

    /**
     * You can setup buckets, assign values separated by ";" with the key
     * 'http.request.buckets'. You can setup precision (application scoped or server
     * scoped), assign a value (max or min) with the key 'http.request.precision'.
     * Default value is "min" (only label is after the context path).
     */
    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        double[] buckets = new double[0];
        if (filterConfig.getInitParameter("http.request.buckets") != null
                && !filterConfig.getInitParameter("http.request.buckets").isEmpty()) {
            final String[] bucketString = filterConfig.getInitParameter("http.request.buckets").split(";");
            buckets = new double[bucketString.length];
            for (int i = 0; i < buckets.length; i++) {
                try {
                    buckets[i] = Double.parseDouble(bucketString[i]);
                } catch (final NumberFormatException e) {
                    LOGGER.error(bucketString[i] + " has no double value. Couldn't initialize SavaFilter.");
                    LOGGER.catching(e);
                    buckets = new double[0];
                    break;
                }
            }
        }
        if (filterConfig.getInitParameter("http.request.precision") != null
                && PathPrecision.MAX.getValue().equals(filterConfig.getInitParameter("http.request.precision"))) {
            pathPrecision = PathPrecision.MAX;
        }
        histogram = SavaUtils.addHistogram("http_requests", "Time and register every http request.", buckets, "path",
                "method");
    }

    /**
     * Precision of stored path.
     */
    private enum PathPrecision {
        /**
         * To get servlet path.
         */
        MIN,
        /**
         * To get request URI, without servlet context.
         */
        MAX;

        /**
         * @return String value.
         */
        private String getValue() {
            return name().toLowerCase();
        }

    }

}
