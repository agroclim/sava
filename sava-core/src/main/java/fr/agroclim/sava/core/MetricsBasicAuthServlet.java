package fr.agroclim.sava.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Base64;

import io.prometheus.client.exporter.MetricsServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

/**
 * SAVA metrics servlet, using basic_auth with context parameters.
 *
 * @author ltromel
 */
@Log4j2
public class MetricsBasicAuthServlet extends MetricsServlet {

    /**
     * UID.
     */
    private static final long serialVersionUID = 8287757024145960994L;

    /**
     * Key, coming from context.
     */
    private String key = null;

    /**
     * Pass, coming from context.
     */
    private String pass = null;

    /**
     * Initialize values to key and pass, coming from context init parameters.
     * @exception ServletException if an exception occurs that interrupts the servlet's normal operation
     */
    @Override
    public void init() throws ServletException {
        super.init();
        key = getServletContext().getInitParameter("sava.key");
        pass = getServletContext().getInitParameter("sava.pass");
        if (key == null || pass == null) {
            throw new ServletException(
                    "SAVA context value ('sava.key', 'sava.pass') were not set. "
                            + "SAVA /metrics servlet cannot be initialized");
        }
        // init values and JVM gauges
        SavaUtils.addCounter("jvm_vendor", "Vendor name of JVM", "", "vendor");
        SavaUtils.addCounter("jvm_version", "Version number of JVM", "", "version");
        SavaUtils.addCounter("tomcat_version", "Version number of Tomcat", "", "version");
        SavaUtils.incrementCounter("jvm_vendor", System.getProperty("java.vm.name"));
        SavaUtils.incrementCounter("jvm_version", System.getProperty("java.vm.version"));
        final String info = getServletContext().getServerInfo();
        SavaUtils.incrementCounter("tomcat_version", info);
        SavaUtils.addGauge("jvm_max_memory",
                "The maximum amount of memory that the virtual machine will attempt to use, measured in bytes");
        SavaUtils.addGauge("jvm_used_memory", "The amount of memory that the virtual machine uses, measured in bytes");
        SavaUtils.addGauge("jvm_total_memory",
                "The total amount of memory in the Java virtual machine, measured in bytes");
        SavaUtils.addGauge("jvm_free_memory",
                "The amount of free memory in the Java Virtual Machine, measured in bytes");
        try (BufferedReader br = new BufferedReader(new InputStreamReader(
                new ProcessBuilder("lsb_release", "-ds").start().getInputStream()))) {
            SavaUtils.addCounter("host_distribution", "Host machine distribution (linux only)", "", "version");
            SavaUtils.incrementCounter("host_distribution", br.readLine());
        } catch (final Exception e) {
            LOGGER.trace("Could not init host_distribution.");
        }
        SavaUtils.addCounter("run_since", "Server run date", "", "instant");
        SavaUtils.incrementCounter("run_since", LocalDateTime.now().toString());

    }

    /**
     * doGet with simple authentification.
     *
     * @param req   an {@link HttpServletRequest} object that contains the request the client has made of the servlet
     * @param resp  an {@link HttpServletResponse} object that contains the response the servlet sends to the client
     * @exception IOException   if an input or output error is detected when the servlet handles the GET request or if
     * the request for the GET could not be handled
     */
    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        // basic_auth associate to the request a header "Authorization"
        if (req.getHeader("Authorization") != null) {
            try {
                // credentials are encoded in Base64, prefixed with "Basic "
                // removing the prefix
                final String trimmed = req.getHeader("Authorization").replace("Basic ", "");
                // decoding the sentence
                final byte[] decodedBytes = Base64.getDecoder().decode(trimmed);
                final String decoded = new String(decodedBytes, StandardCharsets.UTF_8);
                // the credentials are given in the form username:password
                // splitting the sentence
                final String[] decodedSplitted = decoded.split(":");
                // making the checks
                if (decodedSplitted.length == 2 && key.equals(decodedSplitted[0])
                        && pass.equals(decodedSplitted[1])) {
                    // update JVM values
                    final Runtime runtime = Runtime.getRuntime();
                    SavaUtils.setGaugeValue("jvm_max_memory", (double) runtime.maxMemory());
                    SavaUtils.setGaugeValue("jvm_used_memory", (double) (runtime.totalMemory() - runtime.freeMemory()));
                    SavaUtils.setGaugeValue("jvm_total_memory", (double) runtime.totalMemory());
                    SavaUtils.setGaugeValue("jvm_free_memory", (double) runtime.freeMemory());
                    // continue with the servlet
                    super.doGet(req, resp);
                    return;
                }
            } catch (final IOException e) {
                LOGGER.warn("Received a bad request");
                LOGGER.catching(Level.WARN, e);
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
        }
        resp.sendError(HttpServletResponse.SC_FORBIDDEN);
    }

    /**
     * doPost with simple authentification.
     *
     * @param req   an {@link HttpServletRequest} object that contains the request the client has made of the servlet
     * @param resp  an {@link HttpServletResponse} object that contains the response the servlet sends to the client
     * @exception IOException   if an input or output error is detected when the servlet handles the GET request or if
     * the request for the GET could not be handled
     */
    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        resp.sendError(HttpServletResponse.SC_FORBIDDEN);
    }
}
