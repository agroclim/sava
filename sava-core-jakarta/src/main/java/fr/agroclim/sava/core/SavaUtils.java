package fr.agroclim.sava.core;

import java.io.Closeable;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import io.prometheus.client.Collector;
import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.Counter;
import io.prometheus.client.Gauge;
import io.prometheus.client.Histogram;
import io.prometheus.client.Summary;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

/**
 * Utility class to call simplified methods related to the Prometheus Exporter.
 *
 * @author ltromel
 */
@Log4j2
public class SavaUtils {

    /**
     * A map where all registered Collectors are saved, with key=metricName,
     * value=Collector.
     */
    private static final Map<String, Collector> METRICS_MAP = new HashMap<>();

    /**
     * Create and add a {@link Gauge} to registered metrics.
     *
     * @param metricName        Set the name of the metric. Required.
     * @param metricDescription Set the help string of the metric. Required.
     * @param initialValue      Gauge initial value to set. Optional.
     * @param initialValueLabel Label associated with the initial value. Optional.
     * @param labelNames        Label(s) of the metric. Optional. (ex: method)
     */
    public static void addGauge(@NonNull final String metricName, @NonNull final String metricDescription,
            final Double initialValue, final String initialValueLabel, final String... labelNames) {
        final Gauge.Builder gaugeBuilder = Gauge.build().name(metricName).help(metricDescription);
        if (labelNames.length > 0) {
            gaugeBuilder.labelNames(labelNames);
        }
        final Gauge gauge = gaugeBuilder.register(CollectorRegistry.defaultRegistry);
        if (initialValue != null) {
            if (initialValueLabel != null) {
                gauge.labels(initialValueLabel).set(initialValue);
            } else {
                gauge.set(initialValue);
            }
        }
        METRICS_MAP.put(metricName, gauge);
    }

    /**
     * Create and add a {@link Gauge} with an initial value to registered metrics.
     *
     * @param metricName        Set the name of the metric. Required.
     * @param metricDescription Set the help string of the metric. Required.
     * @param initialValue      Gauge initial value to set. Optional.
     */
    public static void addGauge(@NonNull final String metricName, @NonNull final String metricDescription,
            final Double initialValue) {
        addGauge(metricName, metricDescription, initialValue, null);
    }

    /**
     * Create and add an unset {@link Gauge} with labels.
     *
     * @param metricName        Set the name of the metric. Required.
     * @param metricDescription Set the help string of the metric. Required.
     * @param labelNames        Label(s) of the metric. Required. (ex: method)
     */
    public static void addGauge(@NonNull final String metricName, @NonNull final String metricDescription,
            final String... labelNames) {
        addGauge(metricName, metricDescription, null, null, labelNames);
    }

    /**
     * Create and add an unset {@link Gauge} to registered metrics.
     *
     * @param metricName        Set the name of the metric. Required.
     * @param metricDescription Set the help string of the metric. Required.
     */
    public static void addGauge(@NonNull final String metricName, @NonNull final String metricDescription) {
        addGauge(metricName, metricDescription, new String[0]);
    }

    /**
     * Create and add a {@link Gauge} to registered metrics.
     *
     * @param metricName        Set the name of the metric. Required.
     * @param metricDescription Set the help string of the metric. Required.
     * @param getMetric         Method that gets the metric within the Prometheus
     *                          client (must be callable).
     * @param period            Period between 2 actualizations (call of
     *                          getMetrics).
     * @param unit              Time unit of the period.
     */
    public static void addGauge(@NonNull final String metricName, @NonNull final String metricDescription,
            @NonNull final Callable<? extends Object> getMetric, @NonNull final Integer period,
            @NonNull final TimeUnit unit) {
        final Gauge gauge =
                Gauge.build().name(metricName).help(metricDescription).register(CollectorRegistry.defaultRegistry);
        MetricsScheduler.scheduleEveryPeriod(createGaugeActuator(getMetric, gauge), period, unit);
        METRICS_MAP.put(metricName, gauge);
    }

    /**
     * Create and add a labeled {@link Gauge} to registered metrics. Each Gauge with
     * a different label must have a Runnable.
     *
     * @param metricName         Set the name of the metric. Required.
     * @param metricDescription  Set the help string of the metric. Required.
     * @param labelNames         Label(s) of the metric. Required. (ex: method)
     * @param callableLabeledMap A map of <a href=
     *                           "https://prometheus.io/docs/practices/naming/">Label</a>
     *                           (ex: get, post), in association with their method
     *                           that gets the metric within the Prometheus client
     *                           (must be callable).
     * @param period             Period between 2 actualizations (call of
     *                           getMetrics).
     * @param unit               Time unit of the period.
     */
    public static void addGauge(@NonNull final String metricName, @NonNull final String metricDescription,
            @NonNull final Map<String, Callable<? extends Object>> callableLabeledMap, @NonNull final Integer period,
            @NonNull final TimeUnit unit, final String... labelNames) {
        final Gauge gauge = Gauge.build().name(metricName).help(metricDescription).labelNames(labelNames)
                .register(CollectorRegistry.defaultRegistry);
        callableLabeledMap.forEach((label, method) -> MetricsScheduler
                .scheduleEveryPeriod(createLabeledGaugeActuator(method, gauge, label), period, unit));
        METRICS_MAP.put(metricName, gauge);
    }

    private static Runnable createLabeledGaugeActuator(final Callable<? extends Object> method, final Gauge gauge,
            final String label) {
        return () -> {
            try {
                if (label == null) {
                    gauge.set((double) method.call());
                } else {
                    gauge.labels(label).set((double) method.call());
                }
            } catch (final Exception e) {
                LOGGER.warn("Gauge value MUST be a double. Otherwise getMetric.call() has failed."
                        + " Please read stackstrace :");
                LOGGER.catching(e);
            }

        };
    }

    private static Runnable createGaugeActuator(final Callable<? extends Object> getMetric, final Gauge gauge) {
        return createLabeledGaugeActuator(getMetric, gauge, null);
    }

    /**
     * Sets a {@link Gauge} value, if metric's name is registered.
     *
     * @param metricName Set the name of the metric. Required.
     * @param value      Value to set the Gauge.
     */
    public static void setGaugeValue(@NonNull final String metricName, @NonNull final Double value) {
        setGaugeValue(metricName, value, new String[0]);
    }

    /**
     * Sets a {@link Gauge} value, if metric's name is registered.
     *
     * @param metricName Set the name of the metric. Required.
     * @param value      Value to set the Gauge.
     * @param labelNames Label(s) associated. Optional. Requires a Gauge initialized
     *                   with labelNames in the method {@link SavaUtils#addGauge}.
     */
    public static void setGaugeValue(@NonNull final String metricName, @NonNull final Double value,
            final String... labelNames) {
        if (METRICS_MAP.containsKey(metricName) && METRICS_MAP.get(metricName) instanceof Gauge) {
            final Gauge gauge = (Gauge) METRICS_MAP.get(metricName);
            if (labelNames.length > 0) {
                gauge.labels(labelNames).set(value);
            } else {
                gauge.set(value);
            }
        } else {
            LOGGER.warn("setGaugeValue() was not able to set the Gauge value: " + metricName);
        }
    }

    /**
     * Create and add a {@link Counter} to registered metrics.
     *
     * @param metricName        Set the name of the metric. Required.
     * @param metricDescription Set the help string of the metric. Required.
     * @param unit              Unit of the metric. Optional.
     */
    public static void addCounter(@NonNull final String metricName, @NonNull final String metricDescription,
            final String unit) {
        addCounter(metricName, metricDescription, unit, new String[0]);
    }

    /**
     * Create and add a {@link Counter} to registered metrics.
     *
     * @param metricName        Set the name of the metric. Required.
     * @param metricDescription Set the help string of the metric. Required.
     * @param unit              Unit of the metric. Optional.
     * @param labelNames        Label(s) of the metric. Optional. (ex: method)
     */
    public static void addCounter(@NonNull final String metricName, @NonNull final String metricDescription,
            final String unit, final String... labelNames) {
        final Counter.Builder counterBuilder = Counter.build().name(metricName).help(metricDescription);
        if (unit != null) {
            counterBuilder.unit(unit);
        }
        if (labelNames.length > 0) {
            counterBuilder.labelNames(labelNames);
        }
        final Counter counter = counterBuilder.register(CollectorRegistry.defaultRegistry);
        METRICS_MAP.put(metricName, counter);
    }

    /**
     * Increments a {@link Counter} value, if metric's name is registered.
     *
     * @param metricName Set the name of the metric. Required.
     * @param label      Label associated to the measure. Optional.
     */
    public static void incrementCounter(@NonNull final String metricName, final String label) {
        incrementCounter(metricName, label, null);
    }

    /**
     * Increments a {@link Counter} value, if metric's name is registered.
     *
     * @param metricName Set the name of the metric. Required.
     * @param label      Label associated to the measure. Optional.
     * @param amount     Increment by a custom amount. Optional.
     */
    public static void incrementCounter(@NonNull final String metricName, final String label, final Double amount) {
        if (METRICS_MAP.containsKey(metricName) && METRICS_MAP.get(metricName) instanceof Counter) {
            final Counter counter = (Counter) METRICS_MAP.get(metricName);
            if (label != null) {
                if (amount != null) {
                    counter.labels(label).inc(amount);
                } else {
                    counter.labels(label).inc();
                }
            } else {
                if (amount != null) {
                    counter.inc(amount);
                } else {
                    counter.inc();
                }
            }
        } else {
            LOGGER.warn("incrementCounter() was not able to increment the Counter: " + metricName);
        }
    }

    /**
     * Create and add an {@link Histogram} to registered metrics.
     *
     * @param metricName        Set the name of the metric. Required.
     * @param metricDescription Set the help string of the metric. Required.
     */
    public static void addHistogram(@NonNull final String metricName, @NonNull final String metricDescription) {
        addHistogram(metricName, metricDescription, new double[0]);
    }

    /**
     * Create and add an {@link Histogram} to registered metrics.
     *
     * @param metricName        Set the name of the metric. Required.
     * @param metricDescription Set the help string of the metric. Required.
     * @param labelNames        Label(s) of the metric. Optional. (ex: method)
     */
    public static void addHistogram(@NonNull final String metricName, @NonNull final String metricDescription,
            final String... labelNames) {
        addHistogram(metricName, metricDescription, new double[0], labelNames);
    }

    /**
     * Create and add an {@link Histogram} to registered metrics.
     *
     * @param metricName        Set the name of the metric. Required.
     * @param metricDescription Set the help string of the metric. Required.
     * @param buckets           Set the upper bounds of buckets for the histogram.
     *                          The default buckets are intended to cover a typical
     *                          web/rpc request from milliseconds to seconds. They
     *                          can be overridden by setting values to this
     *                          parameter.
     * @param labelNames        Label(s) of the metric. Optional. (ex: method)
     *
     * @return created Histogram.
     */
    public static Histogram addHistogram(@NonNull final String metricName, @NonNull final String metricDescription,
            final double[] buckets, final String... labelNames) {
        final var histogramBuilder = Histogram.build();
        histogramBuilder.name(metricName).help(metricDescription);
        if (buckets.length > 0) {
            histogramBuilder.buckets(buckets);
        }
        if (labelNames.length > 0) {
            histogramBuilder.labelNames(labelNames);
        }
        final Histogram histogram = histogramBuilder.register(CollectorRegistry.defaultRegistry);
        METRICS_MAP.put(metricName, histogram);
        return histogram;
    }

    /**
     * Time a runnable within the named {@link Histogram}.
     *
     * @param metricName The name of the metric. Required.
     * @param callable   Runnable which execution is to be timed.
     */
    public static void actuateHistogram(final String metricName, final Runnable callable) {
        if (METRICS_MAP.containsKey(metricName) && METRICS_MAP.get(metricName) instanceof Histogram) {
            final Histogram histogram = (Histogram) METRICS_MAP.get(metricName);
            histogram.time(callable);
        } else {
            LOGGER.warn("actuateHistogram() was not able to time: " + metricName);
        }
    }

    /**
     * Create and add an {@link Summary} to registered metrics.
     *
     * @param metricName        Set the name of the metric. Required.
     * @param metricDescription Set the help string of the metric. Required.
     * @param ageBuckets        . Optional.
     * @param maxAgeSeconds     . Optional.
     * @param quantileError     The error margin allowed when falling into a
     *                          quantile (see <a href=
     *                          "https://prometheus.io/docs/practices/histograms/#errors-of-quantile-estimation">
     *                          documentation</a>, more details in {@link Summary}).
     *                          Optional.
     * @param quantileValue     The quantile value for this metric. Optional.
     */
    public static void addSummary(@NonNull final String metricName, @NonNull final String metricDescription,
            final Integer ageBuckets, final Integer maxAgeSeconds, final Double quantileError,
            final Double quantileValue) {
        addSummary(metricName, metricDescription, ageBuckets, maxAgeSeconds, quantileError, quantileValue,
                new String[0]);
    }

    /**
     * Create and add an {@link Summary} to registered metrics.
     *
     * @param metricName        Set the name of the metric. Required.
     * @param metricDescription Set the help string of the metric. Required.
     * @param ageBuckets        . Optional.
     * @param maxAgeSeconds     . Optional.
     * @param quantileError     The error margin allowed when falling into a
     *                          quantile (see <a href=
     *                          "https://prometheus.io/docs/practices/histograms/#errors-of-quantile-estimation">
     *                          documentation</a>, more details in {@link Summary}).
     *                          Optional.
     * @param quantileValue     The quantile value for this metric. Optional.
     * @param labelNames        Label(s) of the metric. Optional. (ex: method)
     */
    public static void addSummary(@NonNull final String metricName, @NonNull final String metricDescription,
            final Integer ageBuckets, final Integer maxAgeSeconds, final Double quantileError,
            final Double quantileValue, final String... labelNames) {
        final var summaryBuilder = Summary.build();
        summaryBuilder.name(metricName).help(metricDescription);
        if (ageBuckets != null) {
            summaryBuilder.ageBuckets(ageBuckets);
        }
        if (maxAgeSeconds != null) {
            summaryBuilder.maxAgeSeconds(maxAgeSeconds);
        }
        if (labelNames.length > 0) {
            summaryBuilder.labelNames(labelNames);
        }
        if (quantileError != null || quantileValue != null) {
            Objects.requireNonNull(quantileError);
            Objects.requireNonNull(quantileValue);
            summaryBuilder.quantile(quantileValue, quantileError);
        }
        final Summary summary = summaryBuilder.register(CollectorRegistry.defaultRegistry);
        METRICS_MAP.put(metricName, summary);
    }

    /**
     * Measures a runnable within the named {@link Summary}.
     *
     * @param metricName The name of the metric. Required.
     * @param callable   Runnable which execution is to be timed.
     */
    public static void actuateSummary(@NonNull final String metricName, @NonNull final Runnable callable) {
        actuateSummary(metricName, callable, new String[0]);
    }

    /**
     * Measures a runnable within the named {@link Summary}.
     *
     * @param metricName The name of the metric. Required.
     * @param callable   Runnable which execution is to be timed.
     * @param labels     Label associated to the measure.
     */
    public static void actuateSummary(@NonNull final String metricName, @NonNull final Runnable callable,
            @NonNull final String... labels) {
        if (METRICS_MAP.containsKey(metricName) && METRICS_MAP.get(metricName) instanceof Summary) {
            final Summary summary = (Summary) METRICS_MAP.get(metricName);
            if (labels.length > 0) {
                summary.labels(labels).time(callable);
            } else {
                summary.time(callable);
            }
        } else {
            LOGGER.warn("actuateSummary() was not able to time: " + metricName);
        }
    }

    /**
     * Start a {@link io.prometheus.client.Summary.Timer} for the named
     * {@link Summary}.
     *
     * @param metricName The name of the metric. Required.
     * @return {@link io.prometheus.client.Summary.Timer} created.
     */
    public Summary.Timer summaryStartTimer(@NonNull final String metricName) {
        return summaryStartTimer(metricName, new String[0]);
    }

    /**
     * Start a {@link io.prometheus.client.Summary.Timer} for the named
     * {@link Summary}.
     *
     * @param metricName The name of the metric. Required.
     * @param labels     Label associated to the measure. Optional.
     * @return {@link io.prometheus.client.Summary.Timer} created.
     */
    public Summary.Timer summaryStartTimer(@NonNull final String metricName, final String... labels) {
        if (METRICS_MAP.containsKey(metricName) && METRICS_MAP.get(metricName) instanceof Summary) {
            final Summary summary = (Summary) METRICS_MAP.get(metricName);
            if (labels.length > 0) {
                return summary.labels(labels).startTimer();
            } else {
                return summary.startTimer();
            }
        } else {
            LOGGER.warn("summaryStartTimer() was not able to start the timer: " + metricName);
            return null;
        }
    }

    /**
     * Start a {@link io.prometheus.client.Histogram.Timer} for the named
     * {@link Histogram}.
     *
     * @param metricName The name of the metric. Required.
     * @return The Timer that was started.
     */
    public static Histogram.Timer histogramStartTimer(@NonNull final String metricName) {
        return histogramStartTimer(metricName, new String[0]);
    }

    /**
     * Start a {@link io.prometheus.client.Histogram.Timer} for the named
     * {@link Histogram}, with associated label(s).
     *
     * @param metricName The name of the metric. Required.
     * @param labels     Label associated to the measure. Optional.
     * @return The Timer that was started.
     */
    public static Histogram.Timer histogramStartTimer(@NonNull final String metricName, final String... labels) {
        if (METRICS_MAP.containsKey(metricName) && METRICS_MAP.get(metricName) instanceof Histogram) {
            final Histogram histogram = (Histogram) METRICS_MAP.get(metricName);
            if (labels.length > 0) {
                return histogram.labels(labels).startTimer();
            } else {
                return histogram.startTimer();
            }
        } else {
            LOGGER.warn("histogramStartTimer() was not able to start the timer: " + metricName);
            return null;
        }
    }

    /**
     * Ends the {@link Closeable} (Timer) and register the value of a previously
     * called metric ({@link Histogram} or {@link Summary}).
     *
     * @param timer Timer already started. Required.
     */
    public static void endTimer(@NonNull final Closeable timer) {
        try {
            timer.close();
        } catch (final IOException e) {
            LOGGER.warn("endTimer() was not able to end the timer.");
            LOGGER.catching(e);
        }
    }

    /**
     * Get a Metric from already registered Metrics by name.
     *
     * @param metricName The name of the metric. Required.
     * @return a Metric as Collector interface class.
     */
    public static Collector getMetric(@NonNull final String metricName) {
        return METRICS_MAP.get(metricName);
    }

    /**
     * Gets {@link Gauge} value.
     *
     * @param metricName The name of the metric. Required.
     * @return the Gauge double value
     */
    public static Double getGaugeValue(@NonNull final String metricName) {
        if (METRICS_MAP.containsKey(metricName) && METRICS_MAP.get(metricName) instanceof Gauge) {
            final Gauge gauge = (Gauge) METRICS_MAP.get(metricName);
            return gauge.get();
        } else {
            LOGGER.warn("getGaugeValue() was not able to get the value of the Gauge: " + metricName);
            return null;
        }
    }

}
