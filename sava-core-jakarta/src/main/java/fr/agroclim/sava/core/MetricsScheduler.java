package fr.agroclim.sava.core;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import lombok.NonNull;

/**
 * Manager for scheduled tasks.
 */
public class MetricsScheduler {

    /**
     * Initialize a single thread ScheduledExecutorService to handle
     * scheduled operations.
     */
    private static final ScheduledExecutorService SCHEDULER = Executors.newSingleThreadScheduledExecutor();

    /**
     * Number of seconds in a day.
     */
    private static final int DAY_IN_SECONDS = 60 * 60 * 24;

    /**
     * @param hour   the hour-of-day of delay, from 0 to 23
     * @param minute the minute-of-hour of delay, from 0 to 59
     * @param second the second-of-minute of delay, from 0 to 59
     * @return delays in seconds until the next hour:minute
     */
    static long initialDelay(final int hour, final int minute, final int second) {
        LocalDateTime next = LocalDateTime.of(LocalDate.now(), LocalTime.of(hour, minute, second));
        final LocalDateTime now = LocalDateTime.now();
        if (next.isBefore(now)) {
            next = next.plusDays(1);
        }
        return Duration.between(now, next).getSeconds();
    }

    /**
     * Shut down the scheduler (call on application shutdown).
     */
    public final void shutdownScheduler() {
        SCHEDULER.shutdownNow();
    }

    /**
     * Creates and executes a periodic action.
     *
     * @param runnable the task to execute
     * @param hour     the hour of schedule
     * @param minute   the minute of schedule
     * @param second   the second of schedule
     */
    protected static void scheduleEveryDayAt(final Runnable runnable, final int hour, final int minute,
            final int second) {
        final long initDelay = initialDelay(hour, minute, second);
        SCHEDULER.scheduleAtFixedRate(runnable, initDelay, DAY_IN_SECONDS, TimeUnit.SECONDS);
    }

    /**
     * Creates and executes a periodic action after waiting one period.
     *
     * @param runnable The task to execute.
     * @param period   Period between 2 runs (associated to a unit)
     * @param unit     Unit of the period
     */
    protected static void scheduleEveryPeriod(@NonNull final Runnable runnable, final int period,
            @NonNull final TimeUnit unit) {
        SCHEDULER.scheduleAtFixedRate(runnable, 1, period, unit);
    }

}
