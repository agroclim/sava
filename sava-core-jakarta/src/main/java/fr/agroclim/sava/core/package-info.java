/**
 * Contains all the classes to use SAVA. Aims to facilitate export and exposure
 * of internal data on agroclim java applications, and standardize output
 * addresses for a servlet to expose these values.
 *
 * @author ltromel
 */
package fr.agroclim.sava.core;
